import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { Container, Stack } from "@mui/system";
import SentimentVeryDissatisfiedIcon from "@mui/icons-material/SentimentVeryDissatisfied";

function Crud() {
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [hobby, setHobby] = useState("");
  const [data, setData] = useState([]);
  const [isEdit, setIsEdit] = useState(false);
  const [editIndex, setEditIndex] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleAddressChange = (event) => {
    setAddress(event.target.value);
  };

  const handleHobbyChange = (event) => {
    setHobby(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!name || !address || !hobby) {
      // cek jika ada nilai yang kosong
      alert("Mohon lengkapi semua field.");
      return;
    }

    if (isEdit) {
      const newData = [...data];
      newData[editIndex] = { name, address, hobby };
      setData(newData);
      setIsEdit(false);
      setEditIndex(null);
    } else {
      const newData = { name, address, hobby };
      setData([...data, newData]);
    }
    setName("");
    setAddress("");
    setHobby("");
    handleClose();
  };

  const handleEdit = (index) => {
    setOpen(true);
    const selectedData = data[index];
    setName(selectedData.name);
    setAddress(selectedData.address);
    setHobby(selectedData.hobby);
    setIsEdit(true);
    setEditIndex(index);
  };

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  const filteredData = data.filter((item) =>
    item.name.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            My App
          </Typography>
          <Button variant="contained" color="success" onClick={handleClick}>
            Add User
          </Button>
        </Toolbar>
      </AppBar>
      <Dialog open={open} onClose={handleClose}>
        <form onSubmit={handleSubmit}>
          <DialogTitle align="center">
            {editIndex === null ? "Add User" : "Edit User"}
          </DialogTitle>
          <DialogContent>
            <TextField
              margin="dense"
              label="Name"
              type="text"
              fullWidth
              variant="standard"
              value={name}
              onChange={handleNameChange}
            />
            <TextField
              margin="dense"
              label="Address"
              type="text"
              fullWidth
              variant="standard"
              value={address}
              onChange={handleAddressChange}
            />
            <TextField
              margin="dense"
              label="Hobby"
              type="text"
              fullWidth
              variant="standard"
              value={hobby}
              onChange={handleHobbyChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} variant="contained" color="error">
              Cancel
            </Button>
            <Button type="submit" variant="contained" color="success">
              {editIndex === null ? "Save" : "Update"}
            </Button>
          </DialogActions>
        </form>
      </Dialog>
      {data.length === 0 ? (
        <Stack spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h1" align="center">
              <SentimentVeryDissatisfiedIcon fontSize="inherit" />
            </Typography>
            <Typography variant="h1" align="center" textAlign="center">
              User
            </Typography>
          </Grid>
        </Stack>
      ) : (
        <>
          <Container maxWidth="xl" sx={{ paddingTop: "1rem" }}>
            <TextField
              label="Search by name ..."
              variant="outlined"
              size="small"
              sx={{
                backgroundColor: "#fff",
                boxShadow: "1px 2px 2px -1px #ddd",
              }}
              value={searchTerm}
              onChange={handleSearch}
            />
          </Container>
          {filteredData.map((item, index) => (
            <Container key={index} maxWidth="xl" sx={{ paddingTop: "1rem" }}>
              <Card elevation={2}>
                <CardContent>
                  <Grid
                    container
                    spacing={2}
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <Grid item xs={6} textAlign="left">
                      <Typography variant="h4">{item.name}</Typography>
                      <Typography variant="h6">{item.address}</Typography>
                    </Grid>
                    <Grid item xs={6} textAlign="center">
                      <Typography variant="h4">{item.hobby}</Typography>

                      <Button
                        variant="contained"
                        color="success"
                        onClick={() => handleEdit(index)}
                      >
                        edit
                      </Button>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Container>
          ))}
        </>
      )}
    </>
  );
}

export default Crud;
