# Overview

---

1. Lifecycle pada class component
2. useEffect pada functional component
3. Instalasi dan penggunaan component pada MUI
4. Controlled & Uncontrolled Input pada ReactJS

# Pembahasan Materi

---

Pembahasan materi mengenai

1. Lifecycle pada class component meliputi (componentDidMount, componentDidUpdate, ComponentWillUnmount, dll).
2. Penjelasan mengenai lifecycle pada class component (getDrivedStateFromProps, shouldComponentUpdate, getSnapshotBeforeUpdate)
3. Penjelasan mengenai useEffect, dependensinya, dan fungsi return pada useEffect ketika membuat functional component.
4. Instalasi dan penggunaan component dengan MUI
5. Penjelasan Controlled & Uncontrolled input
