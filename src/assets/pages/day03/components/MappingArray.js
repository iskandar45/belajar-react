import React from "react";

function MappingArray() {
  let array3d = [
    {
      name: "Kadeem Potter",
      phone: "(748) 145-9532",
      address: "172-8475 Aliquam Ave",
    },
    {
      name: "Yetta Hayes",
      phone: "1-207-133-3453",
      address: "981-6927 Sem Avenue",
    },
  ];

  const handlePeserta = () => {
    let dataHandle = array3d ? array3d : [];
    let mappedData = dataHandle.map((item, index) => {
      return (
        <div className="box2" key={index}>
          <p>Nama: {item.name}</p>
          <p>Phone: {item.phone}</p>
          <p>address: {item.address}</p>
          <hr />
        </div>
      );
    });
    return mappedData;
  };
  return handlePeserta;
}

export default MappingArray;
