import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

const LatihanDataDiri = () => {
  return (
    <Card>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Data Diri
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Nama : Nur Imam Iskandar <br />
            Usia : 900 <br />
            Alamat : Karawang <br />
            Message : lorem ipsum dolor
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default LatihanDataDiri;
