import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import React from "react";
import LatihanHeader from "./LatihanHeader";
import LatihanFooter from "./LatihanFooter";
import LatihanDataDiri from "./LatihanDataDiri";
import LatihanTable from "./LatihanTable";
import MappingArray from "./MappingArray";

const MUILayout = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <LatihanHeader />
        </Grid>
        <Grid item xs={12}>
          <LatihanDataDiri />
        </Grid>
        <Grid item xs={12}>
          <MappingArray />
        </Grid>
        <Grid item xs={12}>
          <LatihanTable />
        </Grid>
        <Grid item xs={12}>
          <LatihanFooter />
        </Grid>
      </Grid>
    </Box>
  );
};

export default MUILayout;
