# Overview

1. Student akan mempelajari Apa itu React, dan penggunaannya dengan JSX
2. Student akan mempelajari component berbasis class dan function
3. Student akan mempelajari mengenai state dan props pada class component dan functional component
4. Student akan mempelajari macam-macam cara melakukan styling pada react
5. Student akan mempelajari CSS properties untuk Grid Layout dan Flex Layout