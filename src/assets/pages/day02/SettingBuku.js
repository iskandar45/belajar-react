import React, { useState } from "react";

export const SettingBuku = () => {
  // inputan untuk menampilkan
  // nama buku - penerbit - keluaran taun berapa  - berapa halaman
  // kita bisa mengubah semuanya dengan input
  // ada satu button namanya reset untuk menghapus semua value
  const [nama, setNama] = useState("Pelangi");
  const [penerbit, setPenerbit] = useState("CV Cahaya");
  const [keluaran, setKeluaran] = useState(2022);
  const [page, setPage] = useState(30);

  const standard = () => {
    setNama("Pelangi");
    setPenerbit("CV Cahaya");
    setKeluaran(2022);
    setPage(30);
  };

  const resetBtn = () => {
    setNama("");
    setPenerbit("");
    setKeluaran("");
    setPage("");
    const inputs = document.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = "";
    }
  };

  return (
    <div>
      <h1>SettingBuku</h1>
      <hr />
      <span>Nama Buku: {nama}</span>
      <br />
      <span>Penerbit: {penerbit} </span>
      <br />
      <span>Keluaran Tahun: {keluaran} </span>
      <br />
      <span>Jumlah Halaman: {page} </span>
      <br />
      <button
        onClick={() => {
          standard();
        }}
      >
        Standard
      </button>
      <hr />
      <input
        type={"text"}
        onChange={(e) => {
          setNama(e.target.value);
        }}
        placeholder="ubah nama"
      />
      <input
        type={"text"}
        onChange={(e) => {
          setPenerbit(e.target.value);
        }}
        placeholder="ubah penerbit"
      />
      <input
        type={"number"}
        onChange={(e) => {
          setKeluaran(e.target.value);
        }}
        placeholder="ubah keluaran"
      />
      <input
        type={"number"}
        onChange={(e) => {
          setPage(e.target.value);
        }}
        placeholder="ubah halaman"
      />
      <button
        onClick={() => {
          resetBtn();
        }}
      >
        Reset
      </button>
    </div>
  );
};
