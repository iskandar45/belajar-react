import React, { useState } from "react";

export default function LadingPage() {
  // hook
  // const [first, setfirst] = useState(second)
  // first untuk memanggil variabel
  // setFirst digunakan untuk memanipulasi isi variabel
  const [nama, setNama] = useState("Nur Imam");
  const [umur, setUmur] = useState(900);
  const [alamat, setAlamat] = useState(["alamatku di dekat rumahmu"]);

  const clickFunc = () => {
    setNama("imam");
    setUmur(25);
    setAlamat("Jakarta");
  };

  return (
    <div>
      <h1>Lading Page</h1>
      <p>Ini adalah p tag</p>

      <hr />
      <h2>nama saya: {nama}</h2>
      <h2>umur saya: {umur}</h2>
      <h2>alamat saya: {alamat}</h2>
      <div>
        <p>Input form</p>
        <input
          placeholder="ubah nama"
          onChange={(e) => {
            setNama(e.target.value);
          }}
          type={"text"}
        />
        <input
          placeholder="untuk umur"
          onChange={(e) => {
            setUmur(e.target.value);
          }}
          type={"number"}
        />
        <input
          placeholder="untuk alamat"
          onChange={(e) => {
            setAlamat(e.target.value);
          }}
          type={"text"}
        />
      </div>
      <hr />
      <input />
      <hr />
      <button
        onClick={() => {
          clickFunc();
        }}
      >
        Submit
      </button>
    </div>
  );
}
