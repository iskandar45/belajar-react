import React, { useState } from "react";

export const BukuStatus = () => {
  const defaultNama = "Pelangi";
  const defaultPenerbit = "CV Cahaya";
  const defaultKeluaran = 2022;
  const defaultPage = 30;

  const [nama, setNama] = useState(defaultNama);
  const [penerbit, setPenerbit] = useState(defaultPenerbit);
  const [keluaran, setKeluaran] = useState(defaultKeluaran);
  const [page, setPage] = useState(defaultPage);

  const standard = () => {
    setNama(defaultNama);
    setPenerbit(defaultPenerbit);
    setKeluaran(defaultKeluaran);
    setPage(defaultPage);
  };

  const resetBtn = () => {
    setNama("");
    setPenerbit("");
    setKeluaran("");
    setPage("");
    const inputs = document.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = "";
    }
  };

  const handleNamaChange = (e) => {
    setNama(e.target.value);
  };

  const handlePenerbitChange = (e) => {
    setPenerbit(e.target.value);
  };

  const handleKeluaranChange = (e) => {
    setKeluaran(e.target.value);
  };

  const handlePageChange = (e) => {
    setPage(e.target.value);
  };

  return (
    <div>
      <h1>Buku</h1>
      <hr />
      <p>Nama Buku: {nama}</p>
      <p>Penerbit: {penerbit} </p>
      <p>Keluaran Tahun: {keluaran} </p>
      <p>Jumlah Halaman: {page} </p>
      <button onClick={standard}>Standard</button>
      <hr />
      <input type="text" onChange={handleNamaChange} placeholder="ubah nama" />
      <input type="text" onChange={handlePenerbitChange} placeholder="ubah penerbit" />
      <input type="number" onChange={handleKeluaranChange} placeholder="ubah keluaran" />
      <input type="number" onChange={handlePageChange} placeholder="ubah halaman" />
      <button onClick={resetBtn}>Reset</button>
    </div>
  );
};
