import React from "react";
import Crud from "./assets/components/Crud";

function App() {
  return (
    <div>
      <Crud />
    </div>
  );
}

export default App;
