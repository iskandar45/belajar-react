# React Listing App

Aplikasi ini adalah sebuah React Listing App sederhana yang dapat menampilkan daftar pengguna dan memungkinkan pengguna untuk menambahkan pengguna baru ke dalam daftar.

## Lokasi Script

File `index.js` berisi script utama. Definisi komponen berada pada file `src/components/Crud.js`.

## Fitur

- Menambahkan data user
- Menampilkan data user
- Responsif pada layar dengan ukuran 320px - 1440px
- Menambahkan Search data by name `(New)`
- Menambahkan Edit data `(New)`
- Validasi input data `(New)`

## Teknologi yang Digunakan

- ReactJS
- Material UI

## Instalasi

1. Clone repository ini
2. Buka terminal dan masuk ke dalam direktori project
3. Jalankan perintah `npm install` untuk menginstal semua dependency yang dibutuhkan
4. Jalankan perintah `npm start` untuk menjalankan aplikasi
5. Buka browser dan akses `http://localhost:3000` untuk melihat aplikasi

## Penggunaan

1. Setelah aplikasi dijalankan, daftar pengguna akan ditampilkan.
2. Untuk menambahkan pengguna baru, isi nama pengguna dan email, lalu klik tombol "Add User".
3. Jika ada field yang kosong, maka aplikasi akan menampilkan pesan error.

## License

Proyek ini dilisensikan di bawah lisensi MIT. Silakan lihat file [LICENSE](https://choosealicense.com/licenses/mit/) untuk lebih jelasnya.
